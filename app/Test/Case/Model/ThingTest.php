<?php
App::uses('Thing', 'Model');

/**
 * Thing Test Case
 *
 */
class ThingTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.thing'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Thing = ClassRegistry::init('Thing');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Thing);

		parent::tearDown();
	}

}
