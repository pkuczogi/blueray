<div class="things view">
<h2><?php echo __('Thing'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($thing['Thing']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($thing['Thing']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Photo'); ?></dt>
		<dd>
			<?php echo h($thing['Thing']['photo']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Photo Dir'); ?></dt>
		<dd>
			<?php echo h($thing['Thing']['photo_dir']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($thing['Thing']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($thing['Thing']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Thing'), array('action' => 'edit', $thing['Thing']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Thing'), array('action' => 'delete', $thing['Thing']['id']), null, __('Are you sure you want to delete # %s?', $thing['Thing']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Things'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Thing'), array('action' => 'add')); ?> </li>
	</ul>
</div>
