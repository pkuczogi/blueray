<?php
App::uses('AppController', 'Controller');
/**
 * Things Controller
 *
 * @property Thing $Thing
 * @property PaginatorComponent $Paginator
 */
class ThingsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Thing->recursive = 0;
		$this->set('things', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Thing->exists($id)) {
			throw new NotFoundException(__('Invalid thing'));
		}
		$options = array('conditions' => array('Thing.' . $this->Thing->primaryKey => $id));
		$this->set('thing', $this->Thing->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Thing->create();
			if ($this->Thing->save($this->request->data)) {
				$this->Session->setFlash(__('The thing has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The thing could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Thing->exists($id)) {
			throw new NotFoundException(__('Invalid thing'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Thing->save($this->request->data)) {
				$this->Session->setFlash(__('The thing has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The thing could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Thing.' . $this->Thing->primaryKey => $id));
			$this->request->data = $this->Thing->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Thing->id = $id;
		if (!$this->Thing->exists()) {
			throw new NotFoundException(__('Invalid thing'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Thing->delete()) {
			$this->Session->setFlash(__('The thing has been deleted.'));
		} else {
			$this->Session->setFlash(__('The thing could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}}
